const db = wx.cloud.database();
const testDB = db.collection("test_db");
const _ = db.command;
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  addHandle(){
    testDB.add({
      data:{
        userName:"小明",
        age:25,
        school:"黑工程",
        arr:[1,2,3],
        parent:{
          name:"b"
        }
      }
    }).then((res)=>{
      console.log(res);
    })
  },
  getHandle(){
    testDB.get().then((res)=>{
      console.log(res);
    })
  },
  getByHandle(){
    // testDB.where({
    //   school:"黑工程"
    // }).get().then((res)=>{
    //   console.log(res);
    // })
    // testDB.where({
    //   age:_.gt(21)
    // }).get().then((res)=>{
    //   console.log(res);
    // })
    // testDB.where({
    //   arr:_.in([4])
    // }).get().then((res)=>{
    //   console.log(res);
    // })

    // testDB.where({
    //   "parent.name":"a"
    // }).get().then((res)=>{
    //   console.log(res);
    // })

    // testDB.limit(4).get().then((res)=>{
    //   console.log(res);
    // })

    // testDB.limit(20).skip(0).orderBy("age","asc").get().then((res)=>{
    //   console.log(res);
    // })

    testDB.count().then((res)=>{
      console.log(res);
    })

  },
  delHandle(){
    testDB.doc("79550af260bc3d331d6d2ce441c1efe9").remove().then((res)=>{
      console.log(res);
    })
  },
  editHandle(){
    // testDB.doc("79550af260bc3dd21d6d5a746e55a932").update({
    //   data:{
    //     num:_.inc(1)
    //   }
    // }).then((res)=>{
    //   console.log(res);
    // })

    testDB.doc("b00064a760bc62571e26763d2aa1edc1").update({
      data:{
        arr:_.push(6)
      }
    }).then((res)=>{
      console.log(res);
    })
  },
  editAllHandle(){
    wx.cloud.callFunction({
      name:"viptest"
    }).then((res)=>{
      console.log(res);
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.cloud.callFunction({
    //   name:"viptest",
    //   data:{
    //     school:"黑大",
    //     age:25
    //   }
    // }).then((res)=>{
    //   console.log(res)
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})