// miniprogram/pages/todo_list/todo_list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    todoList:[
      {
        title:"吃饭",
        id:"1001"
      },
      {
        title:"上课",
        id:"1002"
      },{
        title:"学习",
        id:"1003"
      },{
        title:"运动",
        id:"1004"
      },{
        title:"上课",
        id:"1005"
      },{
        title:"学习",
        id:"1006"
      },{
        title:"运动",
        id:"1007"
      },{
        title:"上课",
        id:"1008"
      },{
        title:"学习",
        id:"1009"
      },{
        title:"运动",
        id:"1010"
      },{
        title:"上课",
        id:"1011"
      },{
        title:"学习",
        id:"1012"
      },{
        title:"运动",
        id:"1013"
      }
    ],
    inpVal:""
  },

  // 文本框事件
  inpHandle(e){
    this.setData({
      inpVal:e.detail.value
    })
  },
  // 添加事件
  addHandle(){
    let timer = new Date();
    timer = timer.getTime();
    let _arr = this.data.todoList;
    _arr.push({
      title:this.data.inpVal,
      id:timer
    })
    this.setData({
      todoList:_arr
    })
  },
  // 删除事件
  delHandle(e){
    let _id = e.currentTarget.dataset.id;
    let _arr = this.data.todoList.filter((item)=>{
      if(_id != item.id){
        return true;
      }
    })
    console.log(_arr);
    this.setData({
      todoList:_arr
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: 'https://m.douban.com/rexxar/api/v2/subject_collection/tv_domestic/items?start=5&count=10',
      success:(res)=>{
        console.log(res)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log("pullDown");
    setTimeout(()=>{
      wx.stopPullDownRefresh();
    },500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("hello");
    // wx.showLoading({
    //   title: '加载中...',
    // })
    // setTimeout(()=>{
    //   wx.hideLoading()
    // },500)

    // wx.showToast({
    //   title: 'ok',
    //   duration:3000,
    //   icon:"error"
    // })

    wx.showModal({
      confirmText: 'ok',
      content:"内容",
      title:"标题"
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})