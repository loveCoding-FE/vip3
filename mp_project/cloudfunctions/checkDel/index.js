// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env:"release-a38306"
})

const db = cloud.database()
const todoList = db.collection("todo_list");
const _ = db.command;
// 云函数入口函数
exports.main = async (event, context) => {
  let _ids = event.ids;

  return await todoList.where({
    _id:_.in(_ids)
  }).remove()
}